"""Console script for pyventure_gui."""
import sys
import click
from PySide2.QtWidgets import QApplication, QLabel


@click.command()
@click.argument("label", default="Hello World")
def main(label):
    app = QApplication(sys.argv)
    label = QLabel(label)
    label.show()
    return app.exec_()


if __name__ == "__main__":
    sys.exit(main())  # pragma: no cover
