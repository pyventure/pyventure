"""
A module that offers parsing of Twine story archives and stroring them in a
graph store.

Currently supported formats:
  - Harlowe 3.1.0 (in development)
"""
from bs4 import BeautifulSoup
from copy import copy


class Story(object):
    def __init__(self, title, story_part_tree=None, title_art=None):
        self.title = title
        self.story_part_tree = story_part_tree
        self.currentPosition = story_part_tree
        self.title_art = title_art

    def begin(self):
        return self.story_part_tree

    def show_path(self):
        """
        Returns the path through the story_part_tree that the player has taken so far.

        TODO:
            - Implement!
            - Possible Format Tuple[Nodes, Edges] so player took edge[i] at node[i]
        """
        raise NotImplementedError()


class StoryPart(object):
    """
    This represents a part of a larger story. Every story part consists of:
      - its content,
      - some choices, and
      - (optionally) a piece of background art.

    If the story is to flow linearly to one (and only) one next StoryPart, the
    only choice is "next".

    Members:
        content (str): The content.
        choices (dict): The choices as a mapping of str to StoryPart.
        background: A piece of background art.

    TODO:
        Add networkx backbone
    """

    def __init__(self, content=list(), choices=list(), background_art=list(), id=None):
        self.content = content
        self.choices = choices
        self.background_art = background_art
        self.id = id

    def choose(self, choice: str):
        """
        Returns the StoryPart corresponding to a given choice string. If the
        story is to flow linearly, only one choice "next" is present. If the
        given choice is not an option, None is returned.

        Returns:
            The corresponding choice as a StoryPart or None.
        """
        return self.choices.get(choice)


def load_from_file(path):
    """
    Loads a Twine archive from disk.

    Currently supported formats:
      - Harlowe 3.1.0 (in development)
    """
    with open(path) as file:
        document = "\n".join(file.readlines())
    soup = BeautifulSoup(document, "html.parser")

    # TODO: What if there are multiple stories in the archive?
    title = soup.find_all("tw-storydata")[0]["name"]
    story = Story(title)
    raw_part_content = soup.find_all("tw-passagedata")[0].string
    part_content, choices = parse_raw_part_content(raw_part_content)
    story_part = StoryPart()
    raise NotImplementedError


def parse_raw_part_content(raw_content):
    """
    Parses the raw part content.

    Args:
        raw_content (str): The raw content
        raw_content (str): The raw content

    Returns (tuple):
        A tuple containing
        - a list of Choices
        - a list of content paragraphs that were separated by empty lines
    """

    first_bracket_ix = raw_content.find("[[")
    last_bracket_ix = raw_content.rfind("]]")
    bracket_region = raw_content[first_bracket_ix:last_bracket_ix + 2]
    choices = dict()
    choice_regions = list()

    in_between = False
    opening_ix = None
    for ix, char in enumerate(bracket_region[:-1]):

        # TODO: Improve condition notation and add else branches
        # TODO: MAKE READABLE
        # TODO: Test

        if char == "[" and bracket_region[ix + 1] == "[" and not in_between:
            in_between = True
            opening_ix = ix
        elif char == "]" and bracket_region[ix + 1] == "]" and in_between:
            assert opening_ix is not None
            in_between = False
            # +1 for the second closing square bracket
            choice = bracket_region[opening_ix:ix + 2]
            choice_regions.append((opening_ix, ix + 1))  # Regions are closed intervals!
            choices[choice] = {
                "start": first_bracket_ix + opening_ix,
                "end": first_bracket_ix + ix + 1,
            }
        else:
            pass

    cleaned_part_content = copy(raw_content)
    keep_characters_bitmap = [True] * len(raw_content)
    for _, region in choices.items():
        start = region["start"]
        end = region["end"]
        for ix in range(start, end + 1):
            keep_characters_bitmap[ix] = False
    cleaned_part_content = [
        char for ix, char
        in enumerate(cleaned_part_content)
        if keep_characters_bitmap[ix]
    ]

    return choices, "".join(cleaned_part_content)


def store_on_disk(graph, path):
    """
    Stores a networkx graph to disk.
    """
    raise NotImplementedError
