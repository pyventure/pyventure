import networkx as nx


def test_nx_store():
    graph = nx.DiGraph()
    graph.add_node("Alice")
    graph.add_node("Bob")
    graph.add_edge("Alice", "Bob", relationship="knows")
    assert "Bob" in graph.nodes
    assert "Alice" in graph.nodes

    assert "relationship" in graph.get_edge_data("Alice", "Bob")
    assert graph.get_edge_data("Alice", "Bob")["relationship"] == "knows"
    assert graph.get_edge_data("Bob", "Alice") is None


def test_nx_story_usecase():
    graph = nx.DiGraph()
    intro = "intro"
    intro_content = "bla bla bla SOME CONTENT"
    graph.add_node(intro, content=intro_content)
    first_chapter = "first_chapter"
    graph.add_node(first_chapter, content="bla blubb even more content")
    assert intro in graph
    assert first_chapter in graph
    assert graph.nodes[intro]["content"] == intro_content
