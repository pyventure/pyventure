import pytest
import pyventure_archive.archive as archive
from bs4 import BeautifulSoup


def test_beautiful_soup_interaction():
    example_doc = """
    <html><head><title>The Dormouse's story</title></head>
    <body>
    <p class="title"><b>The Dormouse's story</b></p>

    <p class="story">Once upon a time there were three little sisters; and their names were
    <a href="http://example.com/elsie" class="sister" id="link1">Elsie</a>,
    <a href="http://example.com/lacie" class="sister" id="link2">Lacie</a> and
    <a href="http://example.com/tillie" class="sister" id="link3">Tillie</a>;
    and they lived at the bottom of a well.</p>

    <p class="story">...</p>
    """

    soup = BeautifulSoup(example_doc, "html.parser")

    assert "The Dormouse's story" == soup.title.string
    assert soup.find_all("p")[1] == soup.find_all("p", {"class": "story"})[0]


def test_load_from_file():
    story = archive.load_from_file("tests/stories/The Quest For The Holy Potato.html")
    assert story is not None


@pytest.mark.parametrize(["input", "choices", "cleaned_input"], [
    ("lorem ipsum dolores sid amet", dict(), "lorem ipsum dolores sid amet"),
    ("lorem ipsum[ dolores] sid amet", dict(), "lorem ipsum[ dolores] sid amet"),
    ("lorem ipsum dolores ]]sid [[amet", dict(), "lorem ipsum dolores ]]sid [[amet"),
    ("lorem ipsum dolores ][][sid [[amet", dict(), "lorem ipsum dolores ][][sid [[amet"),
    ("lorem ipsum dolores [[sid ]]amet", {"[[sid ]]": {"start": 20, "end": 27}}, "lorem ipsum dolores amet")
])
def test_parse_raw_part_content(input, choices, cleaned_input):
    # import ipdb
    # ipdb.set_trace()
    assert archive.parse_raw_part_content(input) == (choices, cleaned_input)
