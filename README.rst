=============
PyVenture GUI
=============



.. image:: https://img.shields.io/pypi/v/pyventure_gui.svg
        :target: https://pypi.python.org/pypi/pyventure_gui

.. image:: https://img.shields.io/travis/-/pyventure_gui.svg
        :target: https://travis-ci.org/-/pyventure_gui

.. image:: https://readthedocs.org/projects/pyventure-gui/badge/?version=latest
        :target: https://pyventure-gui.readthedocs.io/en/latest/?badge=latest
        :alt: Documentation Status




PyVenture aims at being a framework for creating simple text adventures. It
concentrates on simplicity - from both a user's standpoint (simple text UI) and
a creator standpoint (simple configuration with Harlowe story
formats, combat system extension points, easy-to-use configuration files).

A game written with support of this library is envisioned to use only:
  - one Harlowe story file, creatable using Twine_, and
  - the pyventure framework
  - (optionally) a title artwork
  - (optionally) further artwork

Having these in place, a simple packaging script provided with the library is
used to package the game into installable and/or runnable executables for all
(or one selected) operating system.

::

    Free Software: MIT license

::

    Documentation: https://pyventure-gui.readthedocs.io.

.. _Twine: https://twinery.org/


Features (TODO)
---------------

* Cross-Platform support (at least MacOS X, Linux, Windows)
* Simple text user interface created with Qt
* Story input capabilities of at least Harlowe 3.1.0
* Simple pre-defined combat system
* Extension points for combat system (Python modules)
* Simple configuration file(s)
* Extensive documentation aimed at creators

Credits
-------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
