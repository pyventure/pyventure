======
Design
======

This directory holds design documents. These can be notes, specifications,
sketches, drafts, recorded texts, and many other things. I want to keep all
important design-related things in one place and with my code, so I can
consult them in a way that is a simple as possible.
