================
Creator Workflow
================

This document shows how I think a creator would want to use the framework. At
least, it shows how I want to use it for my game(s):

1. The creator downloads the framework code (this whole project!).
2. The creator consults the *README_FOR_CREATORS.rst*.
3. The creator puts his Harlowe file into a specific directory.
4. The creator starts the packaging script which opens a nice graphical interface.
5. The creator points the script to his Harlowe story file.
6. The creator points the script to his title art.
7. The creator hits the "test play" button.
8. The creator plays a few scenes.
9. The creator points the script to the desired output directory.
10. The creator is happy and hits the "package" button.
11. In the chosen output directory, the desired executables appear.

While this is the end goal, several iterations before then can include placing
the correct files in the correct directories and stuff like that.

**Implementation Note**::

    The most difficult parts here should be getting a packaging solution to run
    smoothly, as well as implementing the test play.

